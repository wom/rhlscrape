#!/usr/bin/python

import sys
import requests
import bs4
import json

# CONSTANTS
STATES_JSON="stateAbrev.json"
LICENSE_REQ="http://www.myskillsmyfuture.org/Licenses.aspx?onetcode={jobcode}&detailonetcode={jobcode}&keyword={jobname}&highestmatch={jobname}&zipcode={state}&radius=0&workPref=0&showWorkPref=&indgroup=0&indsize=0&TargetRequest=T"
LICENSE_CSS_SEL="span.notranslate a[href^=LicenseDetail]"

# TMP CONSTANTS
# Replace with script arguments
JOB_FILE="soc_names.txt"
STATES=["AL", "CA", "FL"]
OUTFILE="test.dat"

# GLOBALS
jobcodes = {}
states_abv = []


def parseJobCodes(socfile):
    """ Parse the Job Codes file into a {code:title} dictionary.
    """

    # Check for valid soccode file header (do we care?)
    if not "onetsoccode" in socfile.readline():
        return None

    # Remaining lines should be onetsoccodes/job title pairs
    # Ex: "11-1011.00"	"Chief Executives"
    codes = {}
    for line in socfile:
        # Strip quotations/whitespace and also remove -. from job code
        codetitle = line.strip('\"\r\n').split('\"\t\"')
        codes[codetitle[0].translate(None, '-.')] = codetitle[1]

    return codes


def getLicenses(jobcode, jobname, state):
    """ Return a list of known licenses in the state for a particular jobcode. """

    # Use BeautifulSoup to parse response for only License info, ignoring any encoding errors.
    response = requests.get(LICENSE_REQ.format(jobcode=jobcode, jobname=jobname, state=state))
    licenses = [a.text.encode('ascii', 'ignore') for a in bs4.BeautifulSoup(response.text).select(LICENSE_CSS_SEL)]

    #print licenses
    return licenses


#############################################
## MAIN PROCEDURE

from argparse import ArgumentParser

# Argument Parsing
args = ArgumentParser(description="python web scrapping testicle.")
args.add_argument('-j', '--job', type=str,
        help="Specify the job code to search")
args.add_argument('-s', '--state', type=str,
        help="Retrieve licenses for a particular state")
args.add_argument('-o', '--outfile', type=str,
        help="Output filename")
args.add_argument('-c', '--clean', action='store_true', default=False,
        help="Toggle, simple or complex output.")
args.add_argument('--split', action='store_true', default=False,
        help="Split each state into its own output file," \
                "prepending the state's abreviation to outfile name")
options = args.parse_args(sys.argv[1:])

# Load state abreviations from json file
with open(STATES_JSON) as json_file:
    states_abv = json.load(json_file)
    #print str(states)

with open(JOB_FILE) as soc_file:
    jobcodes = parseJobCodes(soc_file)
#print codes

# Setup desired job
jobs = jobcodes
if options.job:
    # Perform lookup for specific job(s),
    # Multiple jobs may be space separated.
    tmpjobs = options.job.translate(None, '.-').split(' ')

    for j in tmpjobs:
        if not jobcodes.get(j):
            print "Job Code ({0}) Invalid!".format(j)
            sys.exit(-1)

    # All jobs valid
    jobs = tmpjobs

# Setup desired states
states = [s['abrev'] for s in states_abv]
if options.state:
    # Multiple states may be space separated.
    tmpstates = options.state.upper().split(' ')

    # Verify states are valid
    for s in tmpstates:
        if not s in states:
            print "Unrecognized State [{0}]! Please use proper US State Abreviations.".format(s)
            sys.exit(-1)

    # All states valid
    states = tmpstates

# Iterate through desired STATES
split_states = options.split
cleanOut = options.clean
outfile = None
for state in states:

    # Open output file for writing if specified
    if not outfile and options.outfile:
        fname = (state + '.' if split_states else '') + options.outfile
        outfile = open(fname, 'w')

        if not outfile:
            print ("Unable to open {0}! Proceding with next state...".format(fname))
            continue

    # Write header
    print "\n\nState: {0} ####################################".format(state)
    if outfile and not split_states and not cleanOut:
        outfile.write("State: {0} ####################################\n".format(state))

    for job in jobs:
        # Lookup job title
        name = jobcodes[job]

        print "Retrieving {2} {1}({0})...".format(job, name, state)
        licenses = getLicenses(job, name, state)
        print "\tLicenses[{0}]:\t{1}".format(len(licenses), "\n\t\t\t".join(licenses) if licenses else "None")
        if outfile:
            if cleanOut:
                outfile.write("{0},{1},{2}\n".format(state, job, len(licenses),))
            else:
                outfile.write("[{0}] {1} {2} - Licenses[{3}]:{4}{5}\n".format(state, job, name, len(licenses), "\n\t\t" if licenses else "", "\n\t\t".join(licenses) if licenses else "None"))

    print >> outfile, ""
    if outfile and split_states:
        outfile.close()
        outfile = None

#Ensure outfile is closed
if outfile and not split_states:
    outfile.close()

## END MAIN
#########################################
