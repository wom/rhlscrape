# README #

python web scrapping testicle.

### Scrape From ###
* https://www.onetonline.org/link/summary/47-2152.02

### soc\_names.txt ### 
* Key/Name pairs. Each correlates to a field and it's oneestcode.
* There are subsets. as in, 

```
#!xls

 "27-2011.00"	"Actors"
 "27-2012.00"	"Producers and Directors"
 "27-2012.01"	"Producers"
 "27-2012.02"	"Directors- Stage, Motion Pictures, Television, and Radio"
 "27-2012.03"	"Program Directors"
 "27-2012.04"	"Talent Directors"
 "27-2012.05"	"Technical Directors/Managers"

```
* Producers and Directors are broken down into i5 other sub types

### What We Need ### 
* State by state; we want every available licences (and their names?) for each title (field).  We have some
  flexiblity in how we want to show this; but Rebeccas initial thought was 1
  file per state listing all available licences.

# How we can get data programatically #
## Example ##

```
#!python

"47-2152.02"    "Plumbers"
http://www.myskillsmyfuture.org/Licenses.aspx?onetcode=47215202&detailonetcode=47215202&keyword=Plumbers&highestmatch=Plumbers&zipcode=AL&radius=0&workPref=0&showWorkPref=&indgroup=0&indsize=0&TargetRequest=T#startcontent
```

* Note: detailonetcode=4721502=onetcode from soc_names, same with keyword
* zipcode=state code (we need al ist of all statees to loop over) 
* What is: workPref, indSize, targetRequest, etc

### Setup ###
* Assumpion of functioning pip
* virtualenv setup

```
#!bash

    Install however for env. On OSX...
        sudo easy_install virtualenv 
    Create new virtual_env...
        mkdir venv
        virtualenv venv/virt1 --no-site-packages
    Switch to new virtual environment..
        source venv/virt1/bin/activate
    Load required packages, in source directory:
        pip install -r requirements.txt
    Check loaded packages
        yolk -l
```

* virtualenv maintenance
    Each time you add a package, be sure to lock changes into requirements via..
    (and notify others to reinitialize from requirements after you push changes)

```
#!bash

        pip freeze > requirements.txt
```

# How to scrape? #
[Beauituful Soup + Requestor Tut](http://blog.miguelgrinberg.com/post/easy-web-scraping-with-python)