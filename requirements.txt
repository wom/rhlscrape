beautifulsoup4==4.3.2
gnureadline==6.3.3
ipython==3.0.0
requests==2.6.0
yolk==0.4.3
